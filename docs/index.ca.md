# Introducció

![](./intro/logo.png)

**Servei d'escriptoris virtuals per a centres públics de Formació Professional de Catalunya.**

El servei d'escriptoris virtuals basat en el programari [IsardVDI](https://isardvdi.com) és un projecte pilot que està donant servei a centres formatius de formació professional. De moment està disponible per a una sèrie de centres que han estat seleccionats i s'anirà ampliant de manera progressiva. Si hi ha dubtes de si un centre està donat d'alta en aquest servei, o vol demanar participar, pot enviar un correu a suport-gencatfp@isardvdi.com

Per a conèixer com s'utilitza el servei i les possibilitats que ofereix s'ha preparat aquesta documentació amb casos d'ús i vídeos que s'aniran incorporant.

El servei de suport inclou que es pugui enviar correus de qualsevol dubte o problema a suport-gencatfp@isardvdi.com.

Per al funcionament més detallat d'Isard es disposa del manual de l'eina en: [https://isard.gitlab.io/isardvdi-docs](https://isard.gitlab.io/isardvdi-docs/index.ca/). Aquí teniu un llistat amb accés directe als apartats destacats d'aquest manual pel professorat:

* [Visors](https://isard.gitlab.io/isardvdi-docs/user/viewers.ca/): Explicació dels tipus de visors avantatges i inconvenients

* [Desplegaments](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/): Desplegaments d'escriptoris com a professor

* [Plantilles](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/): Creació de plantilles

Casos d'ús:

* [Instal·lació de windows 2019](https://isard.gitlab.io/isardvdi-docs-pilot-fp/installacions/guia_instalacio_ws2019/guia_instalacio_ws2019/): pas a pas com s'ha fet la plantilla de windows 2019 per si vols reproduir instal·lacions de windows

* [Xarxes privades i personals](https://isard.gitlab.io/isardvdi-docs-pilot-fp/casos_dus/xarxes_privades_i_personals/xarxes_privades_i_personals/)

* [Exemple d'escriptori amb els diferents tipus de xarxes](https://isard.gitlab.io/isardvdi-docs-pilot-fp/casos_dus/proves_amb_xarxes/proves_amb_xarxes/)


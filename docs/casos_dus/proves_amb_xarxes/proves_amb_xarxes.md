# Proves amb xarxes

Les **xarxes** a IsardVDI dónen lloc a un munt d'**escenaris diferents** per **cada tipus de pràctica**.

En aquest manual s'**expliquen** els **diferents tipus de xarxes** que es poden configurar als escriptoris virtuals i **exemples de casos d'ús** per cada tipus.

Primerament es **fiquen** totes les xarxes a un escriptori i conforme es configuren es **descriu** el seu **funcionament** i **casos d'ús**.


## Manual d'usuari

### Assignació de xarxes a l'escriptori

1.- Es crea un escriptori en base a una plantilla, o en un escriptori ja creat es [modifica l'apartat de Xarxes a Hardware](https://isard.gitlab.io/isardvdi-docs/user/edit_desktop.ca/#hardware), i s'afegeixen **vuit xarxes diferents**

![](./proves_amb_xarxes.images/crear_escriptori.png)

![](./proves_amb_xarxes.images/redes.png)

2.- S'**arrenca** l'escriptori i s'obre un **terminal** on es comproven les 8 xarxes afegides, escrivint ```ip link show```:
```
isard@ubuntu:~$ ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3d:82:1e brd ff:ff:ff:ff:ff:ff
3: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:25:35:1d brd ff:ff:ff:ff:ff:ff
4: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3a:5e:b9 brd ff:ff:ff:ff:ff:ff
5: enp4s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3b:75:c9 brd ff:ff:ff:ff:ff:ff
6: enp5s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:03:c8:83 brd ff:ff:ff:ff:ff:ff
7: enp6s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:41:76:68 brd ff:ff:ff:ff:ff:ff
8: enp7s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:19:cb:5f brd ff:ff:ff:ff:ff:ff
9: enp8s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1366 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:25:de:9f brd ff:ff:ff:ff:ff:ff
```

> NOTA: L’**ordre** de targetes de xarxa que es configuren en l’escriptori a l’Isard és l’ordre de targetes que apareixen configurades al sistema de l’escriptori, per exemple, quan s’escriu al terminal el comandament **ip -c a**


#### Renomenament targetes de xarxa

Per **facilitar** l'experiència i **agilitzar** el treball de l'usuari, es poden **modificar els noms** de les diferents interfícies que corresponen a les diferents xarxes afegides, amb les nomenades **regles udev**.

Les **regles udev** associen el nom del dispositiu original al nom que es volgui modificar.

Es pot fer amb aquest comandament i amb cadascun dels noms descrits al camp ```NAME```:
```
sudo bash -c 'cat > /etc/udev/rules.d/70-persistent-net.rules << EOF
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp1s0", NAME="sortida"
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp2s0", NAME="cirvianum1"
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp3s0", NAME="cirvianum2"
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp4s0", NAME="cirvianum3"
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp5s0", NAME="cirvianum4"
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp6s0", NAME="cirvianum5"
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp7s0", NAME="personal"
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{type}=="1", KERNEL=="enp8s0", NAME="vpn"
EOF'
```
Per aplicar els canvis és necessari fer un **reinici**. 
```
isard@ubuntu:~$ ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: sortida: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3d:82:1e brd ff:ff:ff:ff:ff:ff
    altname enp1s0
3: cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:25:35:1d brd ff:ff:ff:ff:ff:ff
    altname enp2s0
4: cirvianum2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3a:5e:b9 brd ff:ff:ff:ff:ff:ff
    altname enp3s0
5: cirvianum3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:3b:75:c9 brd ff:ff:ff:ff:ff:ff
    altname enp4s0
6: cirvianum4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:03:c8:83 brd ff:ff:ff:ff:ff:ff
    altname enp5s0
7: cirvianum5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:41:76:68 brd ff:ff:ff:ff:ff:ff
    altname enp6s0
8: personal: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:19:cb:5f brd ff:ff:ff:ff:ff:ff
    altname enp7s0
9: vpn: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1366 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether 52:54:00:25:de:9f brd ff:ff:ff:ff:ff:ff
    altname enp8s0
```

> NOTA: Si **posteriorment** es **treuen**, **afegeixen**, o **canvien** l’**ordre** de les targetes corresponents a les interfícies haurem de **tornar a modificar** aquest **fitxer de regles udev**.
Aquesta configuració funciona per a **qualsevol distribució de Linux** actual.


### Configuració de cada targeta i funcionament de cadascuna

Per la explicació es faran servir **dos escriptoris** configurats amb les **mateixes xarxes**, nomenats **equip1** i **equip2**. Hi ficarem les mateixes xarxes a ambdós escriptoris per fer **proves de connexió de targetes** i veure quines **permeten** comunicació i quines no, **com** s'estableix aquesta comunicació, i quines **adreces** es configuren a cadascuna depenent de la xarxa local.

![](./proves_amb_xarxes.images/5YE64YE.png)


#### Xarxa default - sortida a internet

"**Default**" permet als escriptoris **sortida a Internet** amb cada **adreça IP aïllada** de la resta (escriptoris **no es comuniquen**). Es fa servir per defecte a **totes** les plantilles base d'Isard.

Al ser la primera targeta, el DHCP intern d'Ubuntu Desktop li ha creat una configuració (comportament que depèn de cada distribució, no hi ha perquè ser igual a cada escriptori Linux).

```
isard@equip1:~$ ip a s sortida
2: sortida: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:44:60:ce brd ff:ff:ff:ff:ff:ff
    altname enp1s0
    inet 192.168.121.141/22 brd 192.168.123.255 scope global dynamic sortida
       valid_lft 3493sec preferred_lft 3493sec
    inet6 fe80::c184:3254:a9dc:1837/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

isard@equip2:~$ ip a s sortida
2: sortida: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:3d:82:1e brd ff:ff:ff:ff:ff:ff
    altname enp1s0
    inet 192.168.121.176/22 brd 192.168.123.255 scope global dynamic noprefixroute sortida
       valid_lft 3068sec preferred_lft 3068sec
    inet6 fe80::2cd1:aa3f:8fe3:9268/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```


#### Xarxes privades

Les xarxes **privades** estan pensades per muntar escenaris on **diferents usuaris** comparteixen una **mateixa xarxa**. Treballen en un segment d’Ethernet **aïllat** i no hi ha un servidor DHCP que doni adreces IP. Per tant, és recomanable o bé **afegir un servidor DHCP** a aquestes xarxes o configurar l'adreça IP **manualment**.


##### Unmanaged

Per les targetes de xarxa noves que no tinguin una configuració, **NetworkManager** mirarà de crear una nova connexió. 
Per frenar aquest comportament és possible frenar el NetworkManager i el seu treball que fa **automàticament** de crear connexions, i fer la targeta "**unmanaged**".

Una forma de fer-ho és mitjançant regles al fitxer ```/etc/NetworkManager/conf.d/99-unmanaged-devices.conf```: 

```
sudo bash -c 'cat > /etc/NetworkManager/conf.d/99-unmanaged-devices.conf << EOF
[keyfile]
unmanaged-devices=interface-name:cirvianum1;interface-name:cirvianum2
EOF'
```


##### Disabled

Una configuració de xarxa és "**disabled**" quan es **deshabilita del sistema** i no se'n farà ús. 
Es pot fer amb **nmtui**, **nmcli** o amb el gestor de connexions gràfic.

```
nmcli con add con-name cirvianum3 ifname cirvianum3 type ethernet ipv4.method disabled ipv6.method disabled connection.autoconnect yes
nmcli con add con-name cirvianum4 ifname cirvianum4 type ethernet ipv4.method disabled ipv6.method disabled connection.autoconnect yes
```


##### Servidor DHCP i IP dinàmica

Es pot fer aquesta configuració deixant la targeta preparada amb aquest comandament i/o **aprofitar una plantilla** que ja porti un servidor DHCP preparat.

```
sudo nmcli con add con-name cirvianum5 ifname cirvianum5 type ethernet method auto connection.autoconnect yes
```

![](./proves_amb_xarxes.images/aPCbWTY.png)

![](./proves_amb_xarxes.images/I0JdCAn.png)

Es pot **editar el fitxer** de configuració del servidor DHCP i **reiniciar** el servei, perquè agafi la configuració adient per a les targetes i xarxa de la pràctica.


##### Personals

Estan pensades per realitzar pràctiques on l'usuari faci servir un **segment de xarxa exclusiu** entre els **seus escriptoris personals**; no podrà veure els escriptoris de companys encara que estiguin **tots connectats** a aquesta mateixa xarxa "**Personal**".

Es poden deixar configurades plantilles amb adreces IP fixes amb aquesta xarxa i **no col·lisionaran entre usuaris**. 

Per exemple configurarem la ip fixa de la família 172.18.0.101/24:

```
sudo nmcli con add con-name personal ifname personal type ethernet ip4 172.18.0.101/24 connection.autoconnect yes
```


##### VPN

Pensada per treballar **des de casa**, es pot consultar el manual oficial per veure [com es configura la VPN als diferents sistemes del client](https://isard.gitlab.io/isardvdi-docs/user/vpn.ca/#vpn).

És la xarxa que es fa servir pels **visors de RDP**, i té un MTU més petit degut a que s'encapsulen els paquets **xifrats**.


### Verificació de les targetes base

Es fa un reinici i es verifiquen les xarxes.

- "**sortida**" pot sortir a **Internet** independentment:

```
isard@equip11:~$ ip r s default
default via 192.168.120.1 dev sortida proto dhcp metric 102 

isard@equip11:~$ ping -c 1 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=120 time=0.916 ms
```

Es comprova que els dispositius són **gestionats pel NetworkManager** excepte "**cirvianum1**" i "**cirvianum2**":

```
isard@equip11:~$ sudo nmcli device
DEVICE      TYPE      STATE         CONNECTION 
sortida     ethernet  connectat     sortida    
cirvianum5  ethernet  connectat     cirvianum5 
personal    ethernet  connectat     personal   
vpn         ethernet  connectat     vpn        
cirvianum3  ethernet  connectat     cirvianum3 
cirvianum4  ethernet  connectat     cirvianum4 
cirvianum1  ethernet  sense gestió  --         
cirvianum2  ethernet  sense gestió  --         
lo          loopback  sense gestió  -- 
```

El pas final amb aquest escriptori és **crear una plantilla** i així no haver de configurar les diferents xarxes per futures pràctiques.


### Pràctica amb dos escriptoris

Es creen els escriptoris **equip1_8xarxes** i **equip2_8xarxes** en base a la **mateixa plantilla** creada al pas anterior i s'arrenquen juntament amb un escriptori que farà de **servidor DHCP**:

![](./proves_amb_xarxes.images/Z8TDhgC.png)

- Des de l'**equip2** es pot fer ping a la "**Personal**" de l'**equip1**:

```
isard@equip2:~$ ping -c 1 172.18.0.101
PING 172.18.0.101 (172.18.0.101) 56(84) bytes of data.
64 bytes from 172.18.0.101: icmp_seq=1 ttl=64 time=0.038 ms
```

- Es pot accedir a la targeta "**cirvanium5**" de l'**equip1** amb una IP dinàmica:

```
isard@equip2:~$ ip a s cirvianum5
7: cirvianum5: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:0c:ed:38 brd ff:ff:ff:ff:ff:ff
    altname enp6s0
    inet 192.168.220.109/23 brd 192.168.221.255 scope global dynamic noprefixroute cirvianum5
       valid_lft 6844sec preferred_lft 6844sec
    inet6 fe80::fb30:446:f480:f5fd/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever

isard@equip2:~$ ping -c 1 192.168.220.108
PING 192.168.220.108 (192.168.220.108) 56(84) bytes of data.
64 bytes from 192.168.220.108: icmp_seq=1 ttl=64 time=13.1 ms
```

- Es pot configurar una direcció IP **fixa** a "**cirvianum3**" de l'**equip1** mitjançant **entorn gràfic**, per exemple l’adreça 192.168.11.101/24:

![](./proves_amb_xarxes.images/xxqdDlg.png)

- Es fa el mateix a l’**equip2** amb la interfície **nmtui**:

```
sudo nmtui
```

![](./proves_amb_xarxes.images/CYTWdoZ.png)

![](./proves_amb_xarxes.images/Gi7dOkj.png)

![](./proves_amb_xarxes.images/biS9KYy.png)

![](./proves_amb_xarxes.images/UY5cYtd.png)

```
isard@equip2:~$ ip a s cirvianum3
5: cirvianum3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:44:94:bb brd ff:ff:ff:ff:ff:ff
    altname enp4s0
    inet 192.168.11.102/24 brd 192.168.11.255 scope global noprefixroute cirvianum3
       valid_lft forever preferred_lft forever


isard@equip2:~$ ping -c 1 192.168.11.101
PING 192.168.11.101 (192.168.11.101) 56(84) bytes of data.
64 bytes from 192.168.11.101: icmp_seq=1 ttl=64 time=11.0 ms
```

- Finalment, les xarxes **cirvianum1** i **cirvianum2** estan fora del control del NetworkManager i han arrencat ***down***, com es veu aquí amb **cirvanium1**:

```
isard@equip2:~$ ip link show cirvianum1
3: cirvianum1: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000
    link/ether 52:54:00:61:ff:bd brd ff:ff:ff:ff:ff:ff
    altname enp2s0
```

S'activen **manualment** i configuren amb el terminal sense interferències amb el servei de NetworkManager.

S'**aixequen** i configuren dues adreces IP corresponents per a cada interfície, de **diferents families i classes**, i s'efectua la comunicació:

```
isard@equip1:~$ sudo ip link set cirvianum1 up
isard@equip1:~$ sudo ip a a 192.168.21.101/24 dev cirvianum1
isard@equip1:~$ sudo ip a a 172.19.0.101/16 dev cirvianum1
isard@equip1:~$ ip a s cirvianum1
3: cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:7b:79:ee brd ff:ff:ff:ff:ff:ff
    altname enp2s0
    inet 192.168.21.101/24 scope global cirvianum1
       valid_lft forever preferred_lft forever
    inet6 fe80::5054:ff:fe7b:79ee/64 scope link 
       valid_lft forever preferred_lft forever
```

```
isard@equip2:~$ sudo ip link set cirvianum1 up
isard@equip2:~$ sudo ip a a 192.168.21.102/24 dev cirvianum1
isard@equip2:~$ sudo ip a a 172.19.0.102/16 dev cirvianum1
isard@equip2:~$ ip a s cirvianum1
3: cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:61:ff:bd brd ff:ff:ff:ff:ff:ff
    altname enp2s0
    inet 192.168.21.102/24 scope global cirvianum1
       valid_lft forever preferred_lft forever
    inet6 fe80::5054:ff:fe61:ffbd/64 scope link 
       valid_lft forever preferred_lft forever

isard@equip2:~$ ping -c 1 192.168.21.101
PING 192.168.21.101 (192.168.21.101) 56(84) bytes of data.
64 bytes from 192.168.21.101: icmp_seq=1 ttl=64 time=11.1 ms

isard@equip2:~$ ping -c 1 172.19.0.101
PING 172.19.0.101 (172.19.0.101) 56(84) bytes of data.
64 bytes from 172.19.0.101: icmp_seq=1 ttl=64 time=0.263 ms

isard@equip2:~$ sudo ip address flush dev cirvianum1
isard@equip2:~$ ip a s cirvianum1
3: cirvianum1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 52:54:00:61:ff:bd brd ff:ff:ff:ff:ff:ff
    altname enp2s0

```
